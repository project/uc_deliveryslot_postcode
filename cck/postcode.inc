    $content['type']  = array (
    'name' => 'Postcode',
    'type' => 'postcode',
    'description' => 'Add postcodes into our database. We use content types so we can use the full views support.',
    'title_label' => 'City',
    'body_label' => '',
    'min_word_count' => '0',
    'help' => '',
    'node_options' => 
    array (
      'status' => true,
      'promote' => false,
      'sticky' => false,
      'revision' => false,
    ),
    'language_content_type' => '0',
    'old_type' => 'postcode',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'comment' => '0',
    'comment_default_mode' => '4',
    'comment_default_order' => '1',
    'comment_default_per_page' => '50',
    'comment_controls' => '3',
    'comment_anonymous' => 0,
    'comment_subject_field' => '1',
    'comment_preview' => '1',
    'comment_form_location' => '0',
    'skinr_settings' => 
    array (
      'comment_group' => 
      array (
        'acquia_prosper' => 
        array (
          'widgets' => 
          array (
            'fusion-content-alignment' => '',
            'prosper-general-styles' => '',
            'prosper-comments' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'acquia_prosper_hortet' => 
        array (
          'widgets' => 
          array (
            'options' => 
            array (
            ),
            'prosper-general-styles' => '',
            'prosper-comments' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
            'fusion-content-alignment' => '',
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'rootcandy' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
      'node_group' => 
      array (
        'acquia_prosper' => 
        array (
          'widgets' => 
          array (
            'fusion-content-alignment' => '',
            'fusion-float-imagefield' => '',
            'prosper-general-styles' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'acquia_prosper_hortet' => 
        array (
          'widgets' => 
          array (
            'options' => 
            array (
            ),
            'prosper-general-styles' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
            'fusion-content-alignment' => '',
            'fusion-float-imagefield' => '',
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'rootcandy' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
    ),
    'i18n_newnode_current' => 0,
    'i18n_required_node' => 0,
    'i18n_lock_node' => 0,
    'i18n_node' => 1,
    'i18nsync_nodeapi' => 
    array (
      'name' => false,
      'status' => false,
      'promote' => false,
      'moderate' => false,
      'sticky' => false,
      'revision' => false,
      'parent' => false,
      'taxonomy' => false,
      'comment' => false,
      'field_postcode' => false,
      'field_zone' => false,
      'model' => false,
      'list_price' => false,
      'cost' => false,
      'sell_price' => false,
      'weight' => false,
      'weight_units' => false,
      'length' => false,
      'width' => false,
      'height' => false,
      'length_units' => false,
      'pkg_qty' => false,
      'default_qty' => false,
    ),
  );
  $content['fields']  = array (
    0 => 
    array (
      'label' => 'postcode',
      'field_name' => 'field_postcode',
      'type' => 'text',
      'widget_type' => 'text_textfield',
      'change' => 'Change basic information',
      'weight' => 0,
      'rows' => 5,
      'size' => '5',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_postcode][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => NULL,
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'text',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    1 => 
    array (
      'label' => 'Zone',
      'field_name' => 'field_zone',
      'type' => 'nodereference',
      'widget_type' => 'nodereference_select',
      'change' => 'Change basic information',
      'weight' => '1',
      'autocomplete_match' => 'contains',
      'size' => '60',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'nid' => '',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_zone' => 
        array (
          0 => 
          array (
            'nid' => '',
          ),
          'nid' => 
          array (
            'nid' => '',
          ),
        ),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'referenceable_types' => 
      array (
        'zone' => 'zone',
        'product_kit' => 0,
        'image' => 0,
        'news' => 0,
        'postcode' => 0,
        'product' => 0,
        'page' => 0,
        'webform' => 0,
        'vegetables' => 0,
        'panel' => false,
      ),
      'advanced_view' => '--',
      'advanced_view_args' => '',
      'op' => 'Save field settings',
      'module' => 'nodereference',
      'widget_module' => 'nodereference',
      'columns' => 
      array (
        'nid' => 
        array (
          'type' => 'int',
          'unsigned' => true,
          'not null' => false,
          'index' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'revision_information' => '-3',
    'comment_settings' => '-2',
    'menu' => '-4',
    'path' => '-1',
  );


    $content['type']  = array (
    'name' => 'Zone',
    'type' => 'zone',
    'description' => 'A zone where postcodes are assigned to',
    'title_label' => 'Zone',
    'body_label' => '',
    'min_word_count' => '0',
    'help' => '',
    'node_options' => 
    array (
      'status' => false,
      'promote' => false,
      'sticky' => false,
      'revision' => false,
    ),
    'language_content_type' => '0',
    'old_type' => 'zone',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'comment' => '0',
    'comment_default_mode' => '4',
    'comment_default_order' => '1',
    'comment_default_per_page' => '50',
    'comment_controls' => '3',
    'comment_anonymous' => 0,
    'comment_subject_field' => '1',
    'comment_preview' => '1',
    'comment_form_location' => '0',
    'skinr_settings' => 
    array (
      'comment_group' => 
      array (
        'acquia_prosper' => 
        array (
          'widgets' => 
          array (
            'fusion-content-alignment' => '',
            'prosper-general-styles' => '',
            'prosper-comments' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'acquia_prosper_hortet' => 
        array (
          'widgets' => 
          array (
            'options' => 
            array (
            ),
            'prosper-general-styles' => '',
            'prosper-comments' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
            'fusion-content-alignment' => '',
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'rootcandy' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
      'node_group' => 
      array (
        'acquia_prosper' => 
        array (
          'widgets' => 
          array (
            'fusion-content-alignment' => '',
            'fusion-float-imagefield' => '',
            'prosper-general-styles' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'acquia_prosper_hortet' => 
        array (
          'widgets' => 
          array (
            'options' => 
            array (
            ),
            'prosper-general-styles' => '',
            'prosper-common-styles' => 
            array (
              'prosper-gray-border-image' => false,
            ),
            'fusion-content-alignment' => '',
            'fusion-float-imagefield' => '',
          ),
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
        'rootcandy' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
    ),
    'i18n_newnode_current' => 0,
    'i18n_required_node' => 0,
    'i18n_lock_node' => 0,
    'i18n_node' => 1,
    'i18nsync_nodeapi' => 
    array (
      'name' => false,
      'status' => false,
      'promote' => false,
      'moderate' => false,
      'sticky' => false,
      'revision' => false,
      'parent' => false,
      'taxonomy' => false,
      'comment' => false,
      'field_monday' => false,
      'field_tuesday' => false,
      'field_wednesday' => false,
      'field_thursday' => false,
      'field_friday' => false,
      'field_saturday' => false,
      'field_minprice' => false,
      'field_pricelower' => false,
      'field_pricehigher' => false,
      'model' => false,
      'list_price' => false,
      'cost' => false,
      'sell_price' => false,
      'weight' => false,
      'weight_units' => false,
      'length' => false,
      'width' => false,
      'height' => false,
      'length_units' => false,
      'pkg_qty' => false,
      'default_qty' => false,
    ),
  );
  $content['fields']  = array (
    0 => 
    array (
      'label' => 'Monday',
      'field_name' => 'field_monday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '31',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => NULL,
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => NULL,
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    1 => 
    array (
      'label' => 'Tuesday',
      'field_name' => 'field_tuesday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '32',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => NULL,
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_tuesday' => 
        array (
          'value' => 
          array (
            '' => 1,
            '0912' => false,
            1215 => false,
            1618 => false,
            1820 => false,
          ),
        ),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    2 => 
    array (
      'label' => 'Wednesday',
      'field_name' => 'field_wednesday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '33',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_wednesday' => 
        array (
          'value' => 
          array (
            '' => 1,
            '0912' => false,
            1215 => false,
            1618 => false,
            1820 => false,
          ),
        ),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    3 => 
    array (
      'label' => 'Thursday',
      'field_name' => 'field_thursday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '34',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => NULL,
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_thursday' => 
        array (
          'value' => 
          array (
            '' => 1,
            '0912' => false,
            1215 => false,
            1618 => false,
            1820 => false,
          ),
        ),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    4 => 
    array (
      'label' => 'Friday',
      'field_name' => 'field_friday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '35',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_friday' => 
        array (
          'value' => 
          array (
            '' => 1,
            '0912' => false,
            1215 => false,
            1618 => false,
            1820 => false,
          ),
        ),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    5 => 
    array (
      'label' => 'Saturday',
      'field_name' => 'field_saturday',
      'type' => 'text',
      'widget_type' => 'optionwidgets_buttons',
      'change' => 'Change basic information',
      'weight' => '36',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_saturday' => 
        array (
          'value' => 
          array (
            '' => 1,
            '0912' => false,
            1215 => false,
            1618 => false,
            1820 => false,
          ),
        ),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '1',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '0912|9h-12h
  1215|12h-15h
  1618|16h-18h
  1820|18h-20h',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
          'sortable' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    6 => 
    array (
      'label' => 'Minimum Price',
      'field_name' => 'field_minprice',
      'type' => 'number_integer',
      'widget_type' => 'number',
      'change' => 'Change basic information',
      'weight' => '37',
      'description' => '',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_minprice][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_minprice' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_minprice][0][value',
          ),
        ),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'number',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'int',
          'not null' => false,
          'sortable' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    7 => 
    array (
      'label' => 'Price when lower then minimum',
      'field_name' => 'field_pricelower',
      'type' => 'number_integer',
      'widget_type' => 'number',
      'change' => 'Change basic information',
      'weight' => '38',
      'description' => 'Indicates the price that users have to pay if their total amount of the order does not meet the minimum requirements of that zone',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_pricelower][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_pricelower' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_pricelower][0][value',
          ),
        ),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'number',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'int',
          'not null' => false,
          'sortable' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    8 => 
    array (
      'label' => 'Price when higher then minimum',
      'field_name' => 'field_pricehigher',
      'type' => 'number_integer',
      'widget_type' => 'number',
      'change' => 'Change basic information',
      'weight' => '39',
      'description' => 'Indicates the price people have to pay when their total amount of the order is higher then the minimum amount. 
  Normally this is lower then the price when it\'s lower then the minimum',
      'default_value' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_pricehigher][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
        'field_pricehigher' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_pricehigher][0][value',
          ),
        ),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'number',
      'columns' => 
      array (
        'value' => 
        array (
          'type' => 'int',
          'not null' => false,
          'sortable' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'revision_information' => '20',
    'comment_settings' => '30',
    'menu' => '-2',
    'path' => '30',
  );

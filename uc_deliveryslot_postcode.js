/**
 * @file
 * Handle asynchronous calls on checkout page to retrieve delivery pane.
 */

var page;
var details;
var methods;

// Only run if we are in a supported browser.
Drupal.behaviors.setAccess = function (context) {
  var checked = false;
  $("#schedule-wrapper" + " INPUT[type='checkbox']").each(
    function() {
      if($(this).attr('checked') == true) {
        checked = true;
      };
      $(this).bind (
        "click", function() {
                getDeliveryPrice();
        }
      );
    });

  $("#edit-panes-delivery-delivery-postal-code").change(function () {
    setOfflinePayments(false);
  });

  //do our initial price check
  getDeliveryPrice();

}

/**
 * Get shipping calculations for the current cart and line items.
 */
function getDeliveryPrice() {
  var order = serializeOrder();

  if (!!order) {
    $.ajax({
      type: "POST",
      url: Drupal.settings.ucURL.calculateShippingTimeslotPostcode,
      data: 'order=' + Drupal.encodeURIComponent(order),
      dataType: "json",
      error: function (data) {
      // Do any necessary error handling. In this case, I simply
      // ignore errors and keep the current values in place.
      },

      success: function (data) {
        var i;
        var j;
        var key;
        var render = false;
        for (j in data) {
          key = 'delivery_' + data[j].id;
             //Check that this tax is a new line item, or updates its amount.
             if (li_values[key] == undefined || li_values[key] != data[j].amount) {
               //make sure we do not add empty line items
               if(data[j].amount != 0){
                 set_line_item(key, data[j].name, data[j].amount, data[j].weight, data[j].summed, false);
                 render = true;
               } else {
                 remove_line_item(key);
               }
               // Set flag to render all line items at once.
             }

        }

        if (render) {
          //yeha, we made it!
          render_line_items();
        }
      }
    });
  }
}



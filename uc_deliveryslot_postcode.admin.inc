<?php
/**
 * @file
 * Delivery time slot administration menu items and settings.
 *
 */
/**
 * Delivery time slot configuration.
 *
 * @ingroup forms
 * @see uc_deliverytimeslot_admin_view_filter()
 *
 */
function uc_deliveryslot_postcode_admin_view_timetables()
{
  drupal_add_css(drupal_get_path('module', 'uc_deliveryslot_postcode') . '/uc_deliveryslot_postcode.css');
  $weekdays = _uc_deliverytimeslot_week_days();
  //Monday
  $output .= t('Monday');
  $output .= views_embed_view('timetable','block_1');
  //Tuesday
  $output .= t('Tuesday');
  $output .= views_embed_view('timetable','block_2');
  //Wednesday
  $output .= t('Wednesday');
  $output .= views_embed_view('timetable','block_3');
  //Thursday
  $output .= t('Thursday');
  $output .= views_embed_view('timetable','block_4');
  //Friday
  $output .= t('Friday');
  $output .= views_embed_view('timetable','block_5');
  //Friday
  $output .= t('Saturday');
  $output .= views_embed_view('timetable','block_6');

  //if you want to add more:
  //add content field in postcode content type for the specific day
  //add a view block
  //embed it here
  return $output;
}
/**
 * callback for postcodes admin settings
 */
function uc_deliveryslot_postcode_admin_view_postcodes()
{
  drupal_add_css(drupal_get_path('module', 'uc_deliveryslot_postcode') . '/uc_deliveryslot_postcode.css');
  $output = l('Add a postalcode/city','node/add/postcode');
  //also create a menu callback for the deletion of the postalcode and its corresponding data'
  $output .= views_embed_view('cities','block_1');
  return $output;
}

function uc_deliveryslot_postcode_view()
{
  $output = 'view all deliveryhours';
  $output .= views_embed_view('orders');
  return $output;
}
